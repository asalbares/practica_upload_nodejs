// Constantes básicas
const express = require ('express');
// Aquí se crea la aplicación
const app = express();
app.use(express.json());

// Defino un puerto
const port = process.env.PORT || 3000;
// Prepara el puerto para recibir peticiones en el servidor, así le dice al servidor que empiece a escuchar
app.listen(port);

console.log ("API escuchando en el puerto " + port);

// Aquí esperamos una petición GET para recogerla en la ruta dada (URL dada)
app.get("/apitechu/v1/hello",
  // req representa la petición http, y res representa la respuesta
  function(req, res){
    // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
    console.log("GET /apitechu/v1/hello");

    res.send({"msg" : "Hi Antonio from Tech U!"});
  }
)

app.get("/apitechu/v1/users",
  // req representa la petición http, y res representa la respuesta
  function(req, res){
    // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
    console.log("GET /apitechu/v1/users");

    // La variable users recoge los valores del fichero JSON y la variable pasa a ser un array
    var users = require ('./users4.json');
    // La variable usersTop recoge solo los elementos del array que pido en $top
    // para ello utilizo la función array.slice
    var usersTop = users.slice(0,req.query.$top);

    // var i=0;
    // var firstNames={};

    // for (i=0;i<usersTop.length;i++){
    //  console.log (usersTop[i].first_name);
    //  firstNames[i]=usersTop[i].first_name;

    // }

    // Si han pasado el parámetro count, entonces devolver el número de elementos del array totales
    if (req.query.$count=="true"){ //No es un true booleano, es un string, cuidado con ese tema
      var usersResults = {}; //defino un objeto que es el que debo devolver con res.send. El objeto será distinto según count sea true o no
        usersResults.contador = users.length;
        usersResults.usuarios = usersTop;
        // usersResults.nombre = firstNames;
    }else{
      var usersResults = {};
        usersResults.usuarios = usersTop;
        // usersResults.nombre = firstNames;
    }
    res.send({"msg" : usersResults});
    }
)

// Gestiona petición PUSH
app.post("/apitechu/v1/users",

  function (req, res){
    console.log ("POST /apitechu/v1/users");
    console.log (req.headers.first_name);

    // Recoge los valores pasados para crear un nuevo registro en el fichero JSON (users.json)
    var newUser = {
      "first_name" : req.headers.first_name,
      "last_name" : req.headers.last_name,
      "email" : req.headers.email
    }

    var users = require ("./users4.json");

    users.push (newUser);

    writeUserDataToFile (users);

    res.send ("User successfully added to file");

  }
)

// Función de borrado de un registro
app.delete("/apitechu/v1/users/:id",
  function(req,res){
    console.log ("DELETE /apitechu/v1/users/:id");
    console.log ("El ID a borrar enviado es: " + req.params.id);

    // Borra el registro del fichero que digas con el parámetro id que es el orden que ocupa el registro en el fichero JSON
    var users = require ("./users4.json");
    users.splice(req.params.id -1, 1);
    writeUserDataToFile(users);

    console.log ("Usuarios borrado");
  }

)

// Aquí esperamos una petición GET con dos parámetros
app.post("/apitechu/v1/monstruo/:p1/:p2",
  // req representa la petición http, y res representa la respuesta
  function(req, res){
    // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
    console.log("GET /apitechu/v1/hello/monstruo/:p1/:p2");
    console.log ("Parámetros");
    console.log (req.params);

    console.log ("Query String");
    console.log (req.query);

    console.log ("Headers");
    console.log (req.headers);

    console.log ("Body");
    console.log (req.body);
  }
)



// Vamos a refactorizar o reutilizar el código de post anterior para gestionar el fichero
function writeUserDataToFile (data){
  // Aquí defino la constante de librería fs para acceso a ficheros
  const fs = require('fs');

  // Transformo el dato en cadena para poder grabarlo
  var jsonUserData = JSON.stringify (data);

  // Registro el record en el fichero, con formato utf8 de caracteres
  fs.writeFile ("./users4.json", jsonUserData, "utf8",
      function (err){
        if (err){
          console.log (err);
        }else{
          console.log ("Data written to file");
        }
      }
  )

}


// Aquí esperamos una petición POST con un parámetro para que devuelva en consola el contenido del array en la posición que
// el parámetro p1 indique
app.post("/apitechu/v1/access_array/:p1",
  // req representa la petición http, y res representa la respuesta
  function(req, res){
    // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
    console.log("POST /apitechu/v1/access_array/:p1");

    // Vuelca en el objeto users el contenido del fichero users.json
    var users = require ("./users4.json");

    // Muestra el parámetro pasado (p1)
    console.log ("Parámetros");
    console.log (req.params);
    // Muestra el contenido del array users en la posición p1
    console.log (users[req.params.p1].last_name);

  }
)

// Aquí esperamos una petición POST con un parámetro para que devuelva en consola el contenido del array en la posición donde
// el parámetro name sea igual al elemento del array first_name
app.post("/apitechu/v1/look_for_name/:name",
  // req representa la petición http, y res representa la respuesta
  function(req, res){
    // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
    console.log("POST /apitechu/v1/look_for_name/:name");

    // Vuelca en el objeto users el contenido del fichero users4.json
    var users = require ("./users4.json");

    // Muestra el parámetro pasado (p1)
    console.log ("Parámetros");
    console.log (req.params);

    // Encontrar el registro del array según el parámetro usando un bucle for
    // for (intIndex = 0; intIndex < users.length; intIndex++){
    //  if (users[intIndex].first_name == req.params.name){
    //    console.log (users[intIndex].id + " " + users[intIndex].first_name);
    //  break; // Salimos del bucle for si se encuentra el valor en el first_name
    //  }
    // }

    // Encontrar el registro del array según el parámetro usando findIndex
    indexEncontrado = users.findIndex (img => img.first_name == req.params.name);

    // Elimina el registro del array donde el nombre sea el pasado por parámetro
    users.splice(indexEncontrado, 1);
    // Saca el array por pantalla para mostrar que se ha eliminado el registro
    console.log (users);
    // Devuelve a POSTMAN el contenido del array users
    res.send (users);
    // Llama a la función de escritura en el fichero users4.json ya con el registro eliminado
    writeUserDataToFile(users);

    // console.log (indexEncontrado);
    // console.log (users[indexEncontrado].id + " " + users[indexEncontrado].first_name + " " + users[indexEncontrado].last_name);

  }
)

// Aquí esperamos una petición POST con 4 parámetros para insertar un registro en el array y en el fichero
//app.post("/apitechu/v1/insert_array/:p1/:p2/:p3/:p4",
  // req representa la petición http, y res representa la respuesta
  //function(req, res){
    // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
    //console.log("POST /apitechu/v1/access_array/:p1/:p2/:p3/:p4");

    // Vuelca en el objeto users el contenido del fichero users.json
    //var users = require ("./users4.json");

    // Muestra los 4 parámetros pasados (p1, p2, p3 y p4)
    //console.log ("Parámetros");
    //console.log (req.params);

    //lastIndex = users.length;
    //console.log(lastIndex);
    //users
    //console.log(req.params.p2);
    //users[lastIndex].last_name=req.params.p2;
    //users[lastIndex].email=req.params.p3;
    // Llama a la función de escritura en el fichero users4.json ya con el registro insertado
    //console.log (users[lastIndex - 1]);
    //writeUserDataToFile(users);

//  }
//)

app.post("/apitechu/v1/login",
  // req representa la petición http, y res representa la respuesta
  function(req, res){
    // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
    console.log("POST /apitechu/v1/login");

    // Vuelca en el objeto users el contenido del fichero users4.json
    var users = require ("./users4.json");

    // Muestra los parámetros pasados
    console.log ("Parámetros");
    console.log (req.body);

    // Encontrar el registro del array según el parámetro usando findIndex
    indexEncontrado = users.findIndex (img => img.email == req.body.email);

    if (indexEncontrado > -1){
      // console.log ("Lo he encontrado en la posición " + indexEncontrado);
      if (users[indexEncontrado].password==req.body.password){
        console.log ("Password correcta");
        users[indexEncontrado].logged="True";
        console.log (users[indexEncontrado]);
        writeUserDataToFile(users);
        res.send ("Mensaje : Ud está logado");
      }else{
        console.log ("Password incorrecta");
        res.send ("Mensaje : Password incorrecta " + "Usuario ID " + users[indexEncontrado].id);
      }
    }else{
      console.log ("No he encontrado el email");
      res.send ("Mensaje : Login incorrecto");
    }
  }
)

app.post("/apitechu/v1/logout",
  // req representa la petición http, y res representa la respuesta
  function(req, res){
    // Por lo general en las funciones pondremos un console.log para saber donde estamos. Es sólo una traza
    console.log("POST /apitechu/v1/logout");

    // Vuelca en el objeto users el contenido del fichero users4.json
    var users = require ("./users4.json");

    // Muestra los parámetros pasados
    console.log ("Parámetros");
    console.log (req.body);

    // Encontrar el registro del array según el parámetro usando findIndex
    indexEncontrado = users.findIndex (img => img.id == req.body.id);

    if (indexEncontrado > -1){
      // console.log ("Lo he encontrado en la posición " + indexEncontrado);
      if (users[indexEncontrado].logged=="True"){
        console.log ("Logout correcto");
        // Con el siguiente comando elimino la propiedad
        delete users[indexEncontrado].logged;
        //users[indexEncontrado].logged="False";
        console.log (users[indexEncontrado]);
        writeUserDataToFile(users);
        res.send ("Mensaje : Ud está deslogado");
      }else{
        console.log ("ID incorrecto");
        res.send ("Mensaje : ID incorrecto ");
      }
    }else{
      console.log ("No he encontrado el ID");
      res.send ("Mensaje : No he encontrado ID");
    }
  }
)
